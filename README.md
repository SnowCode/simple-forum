# Simple forum

This is a simple forum built-in Flask framework in Python. The main code is less than 100 lines to be as lightweight as possible. 
For the moment the forum is very basic. 

## Features

* Manage accounts
* Order the topics by most active, new and categories
* Like the topics and answers
* Integrate HTML formating (need to change this for security purposes)
* Bumb the posts by reply (the posts are ordered by the last actives)

In the future I want to integrate the following ones as well

* Trust system and badges
* Notifications
* Private msgs
* Change the password and manage the account
* Manage categories and limit them
* Create a search bar to search through, topics and replies

## Why I made it

I made this forum because I want to make a serie of basic lightweight web apps that can be
used to make other projects as well. I challenged myself to make apps with the following lines limits:

* 100 lines for the main *logic*  code (app.py)
* 100 lines for the *non-logic* code (helper.py)
* Not really a limit, but trying to keep it small anyway for the templates. But **not** using external imports. Raw CSS only.
* Other modules are allowed, **only** if they can be used for a various projects (login.py)

So these are the advantages:

* You got a full control over the code because it's simple and basic. You can make it more complex if you want to, but it remmains under your control
* You can use it for education purposes by understanding the code
* You can kickstart your project by integrating this one as a base
* Less lines, less bugs, less troubleshooting :p
* The code is durable, there are less chances that the code becomes obsolete
* Peer to peer friendly, you can adapt the code to make a peer to peer app
* Low-end servers and clients friendly.
* Fast websites. Because the code is limited and no more unneccessary CSS and JS is included the website is much faster

In bonus you can also uses the 'login.py' script to speed up the devloppement of the account management in your own Flask apps.

## How to build the source code

### Using Makefile

Debian:

```bash
git clone https://gitea.com/chopin42/simple-forum
cd simple-forum
sudo make debian
make run
```

Fedora:

```bash
git clone https://gitea.com/chopin42/simple-forum
cd simple-forum
sudo make fedora
make run
```

### From scratch

1. Download the source code:

```bash
git clone https://gitea.com/chopin42/simple-forum
cd simple-forum
```

2. Install the depedencies

Debian:

```bash
sudo apt install python3 python3-pip git
pip3 install -r requirements.txt --user
```

Fedora:

```bash
sudo dnf install python3 python3-pip
pip3 install -r requirements.txt --user
```

3. (Optional) Change the template to fit your needs
4. Run the main file:

```bash
python3 app.py
```

*Note: to change the style of the website I highly recommend you to use the 'Style inspector' built-in in Firefox.*

## How to use and install login.py

You can find the login.py's documentation [here](https://gitea.com/chopin42/login.py).
